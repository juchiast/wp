use cogset::{Euclid, Kmeans};
use std::collections::BTreeMap;
use std::fs;
use std::path::PathBuf;
use strsim::jaro_winkler;

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub wps: BTreeMap<String, String>,
    pub current_wp: Option<String>,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            wps: BTreeMap::new(),
            current_wp: None,
        }
    }
}

impl Config {
    fn config_location() -> PathBuf {
        let mut path = dirs::home_dir().expect("Could not find home dir");
        path.push(".config");
        path.push("wp");
        path.push("config");
        path.set_extension("json");
        path
    }

    pub fn read_config() -> Self {
        match fs::read_to_string(Self::config_location()) {
            Err(_) => Self::default(),
            Ok(json) => match serde_json::from_str(&json) {
                Err(_) => Self::default(),
                Ok(conf) => conf,
            },
        }
    }

    pub fn write_config(&self) {
        let path = Self::config_location();
        let parent = path.parent().unwrap();
        if !parent.is_dir() {
            fs::create_dir_all(parent).expect("Error creating dirs");
        }
        fs::write(&path, serde_json::to_string_pretty(self).unwrap())
            .expect("Error writting config");
    }

    pub fn add_wp(&mut self, name: &str, path: &str) {
        self.wps
            .entry(name.to_owned())
            .and_modify(|e| *e = path.to_owned())
            .or_insert_with(|| path.to_owned());
    }

    pub fn select_wp(&self, name: &str) -> Option<String> {
        let comrade = "☭ ";
        let mut keys: Vec<_> = self.wps.keys().map(String::as_str).collect();
        // Kmeans need at least 3 elements
        for _ in 0..3 {
            keys.push(comrade);
        }
        let points: Vec<_> = keys
            .iter()
            .map(|k| jaro_winkler(k, name))
            .map(|f| Euclid([f, 0.0]))
            .collect();
        let candidate = Kmeans::new(&points, points.len() - 1)
            .clusters()
            .into_iter()
            .map(|(e, idx)| (e.0[0], idx))
            .filter(|(f, _)| f.is_normal())
            .max_by(|(f1, _), (f2, _)| f1.partial_cmp(f2).unwrap());
        let (f, idx) = match candidate {
            Some(x) => x,
            None => return None,
        };
        if f > 0.1 && idx.len() == 1 {
            let s = keys[idx[0]];
            if s == comrade {
                None
            } else {
                Some(s.to_owned())
            }
        } else {
            None
        }
    }

    pub fn set_wp(&mut self, name: &str) -> Option<String> {
        let res = self.select_wp(name);
        if let Some(name) = &res {
            self.current_wp = Some(name.to_owned());
        }
        res
    }

    pub fn del_wp(&mut self, name: &str) -> Option<String> {
        let res = self.select_concrete_wp(name);
        if let Some(name) = &res {
            self.wps.remove(name);
            if self.current_wp.as_ref() == Some(name) {
                self.current_wp = None;
            }
        }
        res
    }

    fn select_concrete_wp(&self, name: &str) -> Option<String> {
        self.wps.keys().find(|&k| k == name).cloned()
    }

    pub fn get_current_wp_path(&self) -> Option<String> {
        self.wps.get(self.current_wp.as_ref()?).cloned()
    }
}
