use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "wp", about = "Workspace manager")]
struct RawApp {
    #[structopt(subcommand)]
    cmd: Option<Command>,
    #[structopt(help = "Workspace name")]
    query: Option<String>,
}

#[derive(StructOpt, Debug)]
enum Command {
    #[structopt(
        name = "launch",
        about = "Launch terminal in current workspace"
    )]
    Launch,
    #[structopt(name = "add", about = "Add new workspace")]
    Add {
        #[structopt(help = "Name of workspace")]
        name: String,
        #[structopt(help = "Path of workspace")]
        path: String,
    },
    #[structopt(name = "del", about = "Delete workspace")]
    Del {
        #[structopt(help = "Name of workspace")]
        query: String,
    },
    #[structopt(name = "set", about = "set active workspace")]
    Set {
        #[structopt(help = "Name of workspace")]
        query: String,
    },
}

pub enum App {
    List,
    Launch,
    Add { name: String, path: String },
    Del { query: String },
    Set { query: String },
    Go { query: String },
}

impl App {
    pub fn from_args() -> Self {
        RawApp::from_args().into()
    }
}

impl From<RawApp> for App {
    fn from(a: RawApp) -> Self {
        match (a.cmd, a.query) {
            (None, None) => App::List,
            (None, Some(query)) => App::Go { query },
            (Some(cmd), None) => match cmd {
                Command::Launch => App::Launch,
                Command::Add { name, path } => App::Add { name, path },
                Command::Del { query } => App::Del { query },
                Command::Set { query } => App::Set { query },
            },
            _ => {
                RawApp::clap().print_help().expect("Error printing help");
                std::process::exit(1);
            }
        }
    }
}
