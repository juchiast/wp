#[macro_use]
extern crate serde_derive;
extern crate cogset;
extern crate dirs;
extern crate serde;
extern crate serde_json;
extern crate strsim;
extern crate structopt;

mod args;
mod config;

use args::App;
use config::Config;
use std::path::Path;

fn main() {
    let arg = App::from_args();
    let mut config = Config::read_config();
    match &arg {
        App::Launch => {
            let fallback = dirs::home_dir()
                .map(|x| x.to_str().unwrap_or("/").to_owned())
                .unwrap_or_else(|| "/".to_owned());
            let dir = match config.get_current_wp_path() {
                Some(path) => if Path::new(&path).is_dir() {
                    path
                } else {
                    eprintln!("warning: current wp doesn't exist");
                    fallback
                },
                None => fallback,
            };
            std::process::Command::new("alacritty")
                .args(&["--working-directory", &dir])
                .spawn()
                .expect("Error launching terminal");
        }
        App::Add { name, path } => {
            config.add_wp(name, path);
            config.write_config();
        }
        App::Del { query } => match config.del_wp(query) {
            Some(name) => {
                config.write_config();
                println!("Deleted workspace {}", name);
            }
            None => eprintln!("Workspace not found"),
        },
        App::Set { query } => match config.set_wp(query) {
            Some(name) => {
                config.write_config();
                println!("Set workspace to {}", name);
            }
            None => eprintln!("Workspace not found"),
        },
        App::Go { query } => match config.select_wp(query) {
            Some(name) => {
                use std::io::Write;
                eprint!("{} ", name);
                let _ = std::io::stderr().flush();
                print!("{}", config.wps[&name]);
                let _ = std::io::stdout().flush();
                eprintln!("");
            }
            None => eprintln!("Workspace not found"),
        },
        App::List => {
            if config.wps.is_empty() {
                eprintln!("Workspace not found");
            } else {
                println!("List of workspaces:");
                for (k, v) in config.wps {
                    match config.current_wp {
                        Some(ref name) if name == &k => {
                            println!("* {}: {}", k, v);
                        }
                        _ => {
                            println!("  {}: {}", k, v);
                        }
                    }
                }
            }
        }
    }
}
